#define _GNU_SOURCE
#define _POSIX_C_SOURCE 200809L
#define SHEEP_DYNARRAY_IMPLEMENTATION
#include "a.h"
#include "dynarray.h"
#include <ftw.h>
EN { TODO, FIXME };
TD S {I t,l,u;B c[256];} NOTE; I i, j; ST B b[4096]; 
I notecmp(C V*a_, C V *b_) {
    C NOTE*a=a_,*b=b_;
    I(a->t-b->t)R a->t-b->t;
    I(a->u-b->u)R b->u-a->u;
    R SQ(a->c,b->c);
}
V dofile(C B*path) {
    I l = 0; NOTE *all = 0;
    FILE *f = fopen(path, "r");
    I(!f,R)
    W(fgets(b,SZ b, f), N ll=strlen(b);I(b[ll-1]=='\n',b[ll-1]=0,l++)
            I u=1,t;B *p,*it;
            I(p=SS(b,"TODO"),t=TODO;it=p+3;W(*it-'O',u++,it++))
            EI(p=SS(b,"FIXME"),t=FIXME;it=p+4;W(*it-'E',u++,it++)) E(CT)
            NOTE c={t,l,u};Mc(c.c,p,min(SZ(c.c)-1,ll-(p-b)));
            arrpush(all,c);
            )
    I(all, qsort(all, arrlen(all), SZ *all, notecmp);
    i(arrlen(all), Pf("%s:%d:%s\n", path, all[i].l, all[i].c);))
    fclose(f); arrfree(all);
}
I dofileftw(C B*p, C S stat *stat, I type, S FTW *b) {
    I(p[b->base]=='.'&&p[b->base+1]!='.'&&p[b->base+1],GT(s))
    dofile(p); r:R FTW_CONTINUE; s:R FTW_SKIP_SUBTREE;
}
V dodir(B*dir) { nftw(dir, dofileftw, 1000, FTW_ACTIONRETVAL); }
I main(I na, B**a) {
    I(na<2,dodir("."))
    E(i(na,dodir(a[i])))
    R 0;
}

